/* Si la température est < 15°C, un cycle de chauffe.
Si la température > 25 °C, le ventilateur tourne en proportion.
Si l’Humidité < 50%, un cycle arrosage.
Si l’Humidité >70 %, le ventilateur tourne en proportion
*/
#define JAUNE      13    // 
#define BLEU       12    //   
#define ROUGE      11    // 
#define VERTE       9    // 
#define APVCC       2    //
#define APGND       6    //
#define AP_H       A0    //
#define AP_T       A1    //

#define IHM
#include "serialTrace.h"  // infos / trace to serial monitor
#include "pt.h"
static struct pt ptT, ptH, ptV, ptC, ptE, ptN, ptA; // proto threads
int temp=0; int humi=0;
bool alarme = false; int nb_cycles = 0;
void log_acqui(char * msg, int va);
void ledPWM(int led, int ratio) {
  int val=constrain(ratio,0,254);   // limit to values [0..254] 
  analogWrite(led, val); 
}
int acquis(int broche, int vmin, int vmax) {
// WIP
  int val ;
  val=analogRead(broche); // 1024 pts, 0 < U < 5V ==> 0 < val < 1023 
  return map (val, 0, 1023, vmin, vmax ) ; 
}
static int ptTemp(struct pt *pt, int repit) {
static unsigned long ttl = 0;
  PT_BEGIN(pt);
  while(1) {
    ttl = millis() + repit ; 
    PT_WAIT_UNTIL(pt, millis() > ttl);
    temp = acquis(AP_T, 0, 50);  // pour ptHumi on a : humi = acquis(AP_T,  20, 80);
    ttyPutInt ("temp: ", temp);
  }
  PT_END(pt);
}
static int ptHumi(struct pt *pt, int repit) {
static unsigned long ttl = 0;
  PT_BEGIN(pt);
  while(1) {
    ttl = millis() + repit; 
    PT_WAIT_UNTIL(pt, millis() > ttl);
    humi = acquis(AP_H,  20, 80);
    ttyPutInt ("humi: ",humi);
  }
  PT_END(pt);
}
static int ptAlarme (struct pt *pt, int duree){
  static unsigned long ttl;
  PT_BEGIN(pt);
  while (alarme) {
    ttl = millis() + duree;
    PT_WAIT_UNTIL(pt, millis() > ttl);
    digitalWrite(VERTE, !digitalRead(VERTE));
  }
  PT_END(pt);
}
static int ptNiveau(struct pt *pt){
  static int nb_old = 0; int ratio;
  PT_BEGIN(pt);
   while (!alarme) {
    nb_old = nb_cycles;
    ratio = map(nb_cycles, 0, 100, 254, 0);
    ledPWM(VERTE, ratio); 
    PT_WAIT_UNTIL(pt, nb_cycles > nb_old ); // attente nouvel arrosage
  }  
  PT_END(pt);
}
static int ptChauffe(struct pt *pt,  int actif, int inactif ){
static unsigned long ttl = 0;
 PT_BEGIN(pt);
   while (1) {
    PT_WAIT_UNTIL(pt,  temp < 15);
    digitalWrite(JAUNE, HIGH);
    ttl = millis() + actif ;
    PT_WAIT_UNTIL (pt, millis() > ttl);
    digitalWrite(JAUNE, LOW);   
    ttl = millis() + inactif ;
    PT_WAIT_UNTIL (pt, millis() > ttl);
  } 
  PT_END(pt);
} // Chauffe
static int ptEau(struct pt *pt,  int actif, int inactif ) {
static unsigned long ttl = 0;
PT_BEGIN(pt);
   while (!alarme) {
    PT_WAIT_UNTIL(pt,  humi < 50);
    digitalWrite(BLEU, HIGH);
    ttl = millis() + actif ;
    nb_cycles++;
    alarme = ( nb_cycles > 100);
    PT_WAIT_UNTIL (pt, millis() > ttl);
    digitalWrite(BLEU, LOW);   
    ttl = millis() + inactif ;
    PT_WAIT_UNTIL (pt, millis() > ttl);
  } 
  PT_END(pt);
} // Eau
static int ptVenti(struct pt *pt) {
  static int temp_old = 0, humi_old = 0; int t_ratio=0, h_ratio = 0, i ;
  static unsigned long ttl = 0;
  PT_BEGIN(pt);
  while (1) {
    if (temp < 25 && humi < 70) digitalWrite(ROUGE, LOW);
    PT_WAIT_UNTIL(pt,  temp > 25 || humi  > 70);
    temp_old = temp;
    humi_old = humi ;
    if (temp > 25) t_ratio = map(temp, 0, 50, 0,254);
    if (humi > 70) h_ratio = map(humi, 30, 100, 0,254);
    i = max(t_ratio , h_ratio );
    ledPWM(ROUGE, i) ;
    PT_WAIT_UNTIL(pt,  temp != temp_old  || humi != humi_old );
  }
  PT_END(pt);
} // Ventilateur

void initIO() {
  pinMode(ROUGE, OUTPUT);    analogWrite(ROUGE, 0);    
  pinMode(JAUNE, OUTPUT);    pinMode(BLEU,  OUTPUT);
  digitalWrite(JAUNE, LOW);  digitalWrite(BLEU, LOW) ;
  pinMode(VERTE, OUTPUT);    analogWrite(VERTE, 254); // at max 
  pinMode(APVCC, OUTPUT);    pinMode(APGND, OUTPUT);
  digitalWrite(APVCC, HIGH); digitalWrite(APGND, LOW);
}
void setup() { 
  int val;
  initIO();                       // Setup: set LEDs & pots
  // static struct pt ptT, ptH, ptV, ptC, ptE, ptN, ptA ; 
  PT_INIT(&ptT); PT_INIT(&ptH); PT_INIT(&ptV); PT_INIT(&ptC);
  PT_INIT(&ptE); PT_INIT(&ptN); PT_INIT(&ptA);
  
  ttySetIHM(9600);
  Serial.println(F("\nLog de " __FILE__ " du " __DATE__));
  val=analogRead(1);
  ttyPutInt("Raw Temp = ",val);
  val=analogRead(0);
  ttyPutInt("Raw humi = ",val);
  temp = acquis(1,  0, 100);
  ttyPutInt("Temp = ",temp);
  humi = acquis(0,  0, 100);
  ttyPutInt("Humi = ",humi);
}

void loop() {  
  ptTemp(&ptT,2000);    // acquisition t° chaque 200ms 
  ptHumi(&ptH,4000);    // acquisition % chaque 500ms
  ptVenti(&ptV);       // pilotage ventilation (LED Rouge)
  ptChauffe(&ptC,5,10); // pilotage chauffage de 3s et inactivité de 3s (LED Jaune)
  ptEau(&ptE,3,20);     // pilotage arrosage de 3s et inactivité de 5s (LED Bleu)
  ptNiveau(&ptN);      // affichage niveau d’eau, PWM Verte de 254 à 0 (LED Verte)
  ptAlarme(&ptA,500);  // alarme réserve vide , clignote à 1Hz  (LED Verte)
}
